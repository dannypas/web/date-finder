<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('session_dates', function (Blueprint $table) {
            $table->id();
            $table->foreignId('date_session_id')->references('id')->on('date_sessions')->onDelete('cascade');
            $table->time('time')->nullable();
            $table->date('date')->index();
            $table->boolean('stamped')->default(false)->index();
            $table->timestamps();

            // Only one date per day per session
            $table->unique(['date_session_id', 'date']);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('session_dates');
    }
};
